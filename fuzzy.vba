Function fuzzy(s1 As String, s2 As String) As Double


  Dim i             As Long
  Dim j             As Long
  Dim Q             As Long
  Dim n1            As Long
  Dim n2            As Long
  Dim Tn1           As Double
  Dim Tn2           As Double

  i = 1
  n1 = Len(s1)
  n2 = Len(s2)

  If n1 > 0 And n2 > 0 Then
    Do While i <= n1
      j = 1
      Do While j <= n1 - i + 1
        If InStr(1, s2, Mid(s1, i, j), vbTextCompare) Then Q = Q + j
        j = j + 1
      Loop
      i = i + 1
    Loop

    Tn1 = n1 * (n1 + 1) * (n1 + 2) / 6
    Tn2 = n2 * (n2 + 1) * (n2 + 2) / 6
    fuzzy = (n1 * Q / Tn1 + n2 * Q / Tn2) / (n1 + n2)
  End If
End Function
